---
layout: case-study
lang: en
visible_home: true
title: Vota Inteligente | Case Study | Lissett García Albornoz
permalink: /case-studies/vota-inteligente/

name: Vota Inteligente
subtitle: ¿Cómo hacer partícipe a la ciudadanía en la construcción de los programas electorales?

category: Proyecto de Fundación Ciudadanía Inteligente
country: Implementado en Chile

role: A cargo de
area_01: Investigación de usuarios
area_02: Diseño, prototipado y testeo web
area_03: Desarrollo Front-end
area_04: Diseño Gráfico
area_05: Diseño Editorial
area_05: facilitación

cover_image: "/assets/images/cover-vota.jpg"
hero_image_desktop: "/assets/images/casestudies/vota/13-02.png"
hero_image_mobile: "/assets/images/welcome-vota.jpg"
---

<div class="uk-width-1-4@m uk-text-right@m">
    <p class="section-title">Contexto y desafío</p>
</div>
<div class="uk-width-3-4@m">
    <p class="project-content">Vota Inteligente (VI) es el primer proyecto de la Fundación Ciudadanía Inteligente (FCI). Desde su primera versión en 2009, se construyó como una herramienta digital para informar a la ciudadanía sobre los distintos programas de las candidaturas presidenciales, poniendo la tecnología a disposición de la política, fomentando el voto informado. En el contexto de las elecciones presidenciales y parlamentarias 2017, desde la dirección de FCI, se decidió reformular el proyecto dada la baja participación electoral que venía presentando el país en los últimos años por lo que VI 2017 se enfocó en investigar ese conflicto y encontrar espacios que promovieran la participación de la ciudadanía durante el proceso electoral. </p>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Equipo y metodología</p>
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">Conformamos un equipo interdisciplinario compuesto por periodistas, sociólogos, abogados, desarrolladores y diseñadores. Tuvimos los recursos para realizar una fuerte investigación de usuarios de forma permanente durante todo el año que duró el proyecto. Dada la complejidad de VI tomamos distintos elementos de las metodologías <span>Human Centered Design</span>, <span>Agile Software Development</span> y <span>Lean UX</span> para guiar nuestra investigación, desarrollo y ejecución.</p>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Investigación, métodos y aprendizajes</p>
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">Para comenzar, aterrizamos nuestros conocimientos y supuestos, en base a estudios, experiencias y observaciones. El primer paso fue definir métodos de investigación y validar nuestros supuestos para reformular nuestros conocimientos. Definimos una estrategia inicial de inmersión con entrevistas individuales, encuentros de conversación grupales y entrevistas a expertos. Para esto armamos proto personas, segmentamos por grupos de interés y enlistamos expertos relevantes, respectivamente, según distintos roles en el proceso electoral.</p>
</div>
<div class="uk-width-1-1@m">
  <img src="/assets/images/casestudies/vota/04-1.jpeg" alt=""/>
</div>
<div class="uk-width-1-2@m">
  <img src="/assets/images/casestudies/vota/04-2-A.jpg" alt=""/>
</div>
<div class="uk-width-1-2@m">
  <img src="/assets/images/casestudies/vota/04-2-B.jpg" alt=""/>
</div>
<div class="uk-width-1-4@m uk-text-right@m">
</div>
<div class="uk-width-3-4@m" uk-grid>
  <p class="project-content">A través de mapas de afinidad detectamos grietas y <span> 6 áreas de intervención</span>, las cuales llevamos de vuelta a grupos de conversación con usuarios y organizaciones aliadas para priorizar desafíos y definir productos concretos a desarrollar y testear.</p>
  <div class="uk-width-1-1@m">
    <span>Grietas</span>
    <ul class="project-content">
      <li>A la ciudadanía sí le interesa lo político</li>
      <li>Hay ideas políticas en la ciudadanía</li>
      <li>La ciudadana quiere incidir</li>
      <li>La ciudadana articulada tiene el potencial de tomarse la agenda</li>
    </ul>
  </div>
</div>


<div class="uk-width-1-1@m">
  <img src="/assets/images/casestudies/vota/04-3.png" alt=""/>
</div>
<div class="uk-width-1-4@m uk-text-right@m">
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">Estas áreas nos permitieron enfocar nuestros desafíos a través de preguntas de Diseño:</p>
  <ul>
  <li><span>Sentir:</span> ¿Cómo podríamos articular a personas que compartan un interés individual? ¿Cómo asegurar que su opinión influya?</li>
  <li><span>Espacios de participación ciudadana:</span> ¿Cómo generamos más encuentros cara a cara? ¿Cómo podemos rescatar las ideas políticas de los lugares donde las personas se sienten cómodas?</li>
  <li><span>Ser político y NNA:</span> ¿Cómo aprovechamos el potencial de niños, niñas y adolescentes para más padres y madres participen? ¿Cómo llegamos a las salas de clases? </li>
  <li><span>Barreras participativas:</span> ¿Cómo facilitar la participación de la ciudadanía? ¿Cómo generar espacios donde se sientan cómodos?</li>
  </ul>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Estrategia de diseño de productos</p>
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">Como desafío prioritario definimos la interacción de creación de propuestas. Es decir, <span>la plataforma consistiría en un medio que facilitara la creación y recolección de propuestas ciudadanas, las cuales las candidaturas podrían conocer e incorporar a sus programas comprometiéndose a implementarlas en caso de salir electas</span>. Lideré este proceso para construir de forma colaborativa la estructura, prototipo y diseño de las distintas versiones de la plataforma, las cuales cambiarían durante el año según los aprendizajes en el camino y los distintos hitos políticos de las elecciones.</p>
</div>
<div class="uk-width-1-1@m">
  <img src="/assets/images/casestudies/vota/esquema.png" alt=""/>
</div>
<div class="uk-width-1-2@m">
  <img src="/assets/images/casestudies/vota/06-1-A.jpg" alt=""/>
</div>
<div class="uk-width-1-2@m">
  <img src="/assets/images/casestudies/vota/06-1-B.jpg" alt=""/>
</div>
<div class="uk-width-1-1@m">
  <img src="/assets/images/casestudies/vota/06-2.png" alt=""/>
</div>
<div class="uk-width-1-4@m uk-text-right@m">
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">Ya con una primera versión mínima viable de la web que recogía propuestas ciudadanas, uno de los principales problemas detectados a través del trabajo colaborativo con organizaciones aliadas fue la falta de apoyo teórico para formular propuestas, por lo que diseñé y testeamos en conjunto con grupos de ciudadanos y metodólogos una metodología para la creación de propuestas amigable y fácil de realizar por grupos autoconvocados. Finalmente, más adelante se terminarían incorporando 3 manuales de apoyo opcional, disponibles para descargar en la plataforma: Guía de Elaboración de propuestas ciudadanas, Manual de Formación Ciudadana y Manual de Herramientas de Activismo, los cuales tuvieron distintas versiones según las mejoras que fuimos agregando.</p>
  <img src="/assets/images/casestudies/vota/06-3-A.jpg" alt=""/>
</div>
<div class="uk-width-1-4@m">
</div>
<div class="uk-width-1-4@m">
  <img src="/assets/images/casestudies/vota/06-3-B.jpg" alt=""/>
</div>
<div class="uk-width-1-4@m">
  <img src="/assets/images/casestudies/vota/06-3-C.jpg" alt=""/>
</div>
<div class="uk-width-1-4@m">
  <img src="/assets/images/casestudies/vota/06-3-D.jpg" alt=""/>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Objetivos y metas</p>
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">En base al trabajo colaborativo y proyectos anteriores de la fundación con similar alcance, estimamos las metas del proyecto. El objetivo fue hacer que las ideas políticas de ciudadanos, ciudadanas,  organizaciones, instituciones y movimientos sociales de todo el territorio nacional incidan en la agenda de las candidaturas al Congreso y Presidencia de Chile en el marco de las Elecciones 2017. A partir de este objetivo, las metas fueron las siguientes:
  <ul>
    <li>Lograr 500 propuestas ciudadanas en la plataforma</li>
    <li>Lograr 40 apadrinamientos por parte de organizaciones a propuestas de personas particulares o de encuentros ciudadanos</li>
    <li>Lograr 300 propuestas surgidas desde encuentros ciudadanos</li>
    <li>Lograr compromisos con propuestas de 225 candidatos (20%)</li>
  </ul>
</p>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Primer lanzamiento</p>
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">Si bien la plataforma aún podía seguir mejorando, decidimos salir con un primer lanzamiento a modo de dar a conocer el sitio y convocar a grupos de ciudadanos para que se organizaran en sus comunidades y levantaran propuestas. Para dar más visibilidad al proyecto realizamos una intervención en el centro de Santiago y aprovechamos la instancia para tener más conversaciones con la ciudadanía y mejorar las interacciones de la plataforma. El llamado era "¿Si fueras presidente o presidenta, qué es lo primero que harías por Chile?"</p>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/07-1-A.jpg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/07-1-B.jpg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/07-1-C.jpg" alt=""/>
</div>
<div class="uk-width-1-1@m">
  <img src="/assets/images/casestudies/vota/07-2.png" alt=""/>
</div>
<div class="uk-width-1-4@m uk-text-right@m">
</div>
<div class="uk-width-3-4@m">
  <p class="project-content">Iniciamos una fuerte campaña convocando a encuentros en todo el territorio nacional de la mano de organizaciones territoriales. Participé de la facilitación de algunos encuentros ciudadanos lo que permitió ver de primera fuente nuevas mejoras tanto en la plataforma como en los manuales metodológicos.</p>
  <img style="margin-bottom: 30px" src="/assets/images/casestudies/vota/08-2.jpeg" alt=""/>
  <img style="margin-bottom: 30px" src="/assets/images/casestudies/vota/08-1-A.jpg" alt=""/>
  <img src="/assets/images/casestudies/vota/08-1-B.jpg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/08-3-A.jpeg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/08-3-B.jpeg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/08-3-C.jpeg" alt=""/>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Segundo lanzamiento</p>
</div>
<div class="uk-width-3-4@m" uk-grid>
  <p class="project-content">El segundo gran lanzamiento del año, fue la publicación oficial de las candidaturas presidenciales y parlamentarias. Este lanzamiento re-estructuró todo el sitio ya que implicó el desarrollo de nuevas secciones y features en la plataforma: perfiles de candidaturas, compromisos de las candidaturas y la media naranja, un match electoral. Estas secciones fueron lanzadas como mínimo viable y mejoradas a través de los meses. Además, incorporamos nuevas interacciones para fomentar la participación de los usuarios que ya habían subido sus propuestas a la plataforma, de modo que se mantuvieran activos en el proceso de recolección de compromisos de candidaturas con sus propuestas.</p>
  <img src="/assets/images/casestudies/vota/09-3-C.jpg" alt=""/>
  <div class="uk-width-1-2@m">
    <img src="/assets/images/casestudies/vota/09-3-A.jpg" alt=""/>
  </div>
  <div class="uk-width-1-2@m">
    <img src="/assets/images/casestudies/vota/09-3-B.jpg" alt=""/>
  </div>
</div>
<div class="uk-width-1-1@m">
  <img src="/assets/images/casestudies/vota/13-02.png" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/13-01-A.png" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/13-01-B.png" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/13-01-C.png" alt=""/>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Tercer gran lanzamiento y bonus: Vota Kids</p>
</div>
<div class="uk-width-3-4@m" uk-grid>
  <p class="project-content">Uno de los productos más inesperados del proyecto fue la realización de Vota Inteligente en su versión para niños, niñas y adolescentes (NNA).Área de intervención que fue mencionada y priorizada en la investigación inicial. Esta versión surgió durante los últimos 2 meses de actividad del proyecto, donde ya con el sitio de VI construido casi en su totalidad, realizamos una alianza con UNICEF Chile para adaptar la metodología de encuentros ciudadanos para NNA y la plataforma. Utilizamos la misma estructura adapta a una estética más lúdica dirigida a NNA y facilitamos material de apoyo para que profesores replicaran los encuentros en las escuelas. Este proyecto alternativo lo nombramos "Levanta la Mano" y lo lideré completamente, tanto en diseño, implementación, adaptación de la metodología y observación en uso en colegios. En 2 meses logramos 211 propuestas de NNA en 12 regiones del país y 38 compromisos de candidaturas con sus propuestas. Puedes ver este proyecto en detalle <a href="https://lissteilor.design/rise-your-hand/" target="_blank">acá</a>. </p>
</div>
<div class="uk-width-1-2@m">
  <img src="/assets/images/casestudies/vota/12-02-A.jpg" alt=""/>
</div>
<div class="uk-width-1-2@m">
  <img src="/assets/images/casestudies/vota/12-02-B.jpg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/12-03-A.jpeg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/12-03-B.jpeg" alt=""/>
</div>
<div class="uk-width-1-3@m">
  <img src="/assets/images/casestudies/vota/12-03-C.jpeg" alt=""/>
</div>

<div class="uk-width-1-4@m uk-text-right@m">
  <p class="section-title">Cierre del proyecto, evaluación y proyección</p>
</div>
<div class="uk-width-3-4@m" uk-grid>
  <p class="project-content">Al término del proyecto, ya con las candidaturas electas y con el foco en el candidato presidencial electo, actualizamos nuevamente el sitio visibilizando los 15 compromisos presidenciales. En cuento a las metas esperadas quedaron de la siguiente manera:
  <ul>
    <li>895 (684 + 211)/500 propuestas ciudadanas en la plataforma</li>
    <li>112/40 apadrinamientos por parte de organizaciones a propuestas de personas particulares o de encuentros ciudadanos</li>
    <li>117/300 propuestas surgidas desde encuentros ciudadanos</li>
    <li>215 (188+19+8) /225 candidatos comprometidos con al menos una propuesta</li>
  </ul>
  Lo que nos dejó el desafío de seguir acercando la relación de la ciudadanía con las candidaturas.
</p>
</div>
<div class="uk-width-1-1@m">
  <img src="/assets/images/casestudies/vota/14.png" alt=""/>
</div>
