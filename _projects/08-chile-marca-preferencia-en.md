---
layout: project
lang: en
visible_home: true
title: Chile Marks a Preference | Lissett García Albornoz
permalink: /chile-marks-a-preference/

label: Openness & Transparency
name: Chile Marks a Preference
subtitle: Positions of political actors on important issues for Chile

versions: Year
years: '2020'
category: Implemented by Ciudadanía Inteligente in
country: Chile

lead:
tag_01: art
tag_02: web
tag_03: frontend
tag_04: info
tag_05: graphic

role: My role
area_01: Art Direction
area_02: User Experience (UX)
area_03: Web Design & Prototyping
area_04: Front-end Development
area_05: Infographic
area_06: Graphic Design

cover_image: "/assets/images/cover-chilemarca.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Web platform"
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/screen-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/screen-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/screen-03.jpg"
  - title:
    description:
    columns: 2
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/view-01.png"
         - url: "/assets/images/chilemarca/view-02.png"
  - title: "Graphics to share results "
    description:
    columns: 2
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/share-01.png"
         - url: "/assets/images/chilemarca/share-02.png"
  - title: "Graphics"
    description:
    columns: 2
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/gif-01.gif"
         - url: "/assets/images/chilemarca/gif-02.gif"
  - title:
    description:
    columns: 3
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/graphic-01.jpg"
         - url: "/assets/images/chilemarca/graphic-02.jpg"
         - url: "/assets/images/chilemarca/graphic-03.jpg"
  - title:
    description:
    columns: 3
    images:
         - url: "/assets/images/chilemarca/graphic-04.jpg"
         - url: "/assets/images/chilemarca/graphic-05.jpg"
         - url: "/assets/images/chilemarca/graphic-06.jpg"
---
