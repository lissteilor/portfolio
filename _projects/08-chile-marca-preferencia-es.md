---
layout: project
lang: es
visible_home: true
title: Chile Marca Preferencia | Lissett García Albornoz
permalink: /chile-marca-preferencia-es/

label: Apertura y Transparencia
name: Chile Marca Preferencia
subtitle: Posiciones de actores políticos sobre temas relevantes para Chile

versions: Año
years: '2020'
category: Implementado por Ciudadanía Inteligente en
country: Chile

lead:
tag_01: art
tag_02: web
tag_03: frontend
tag_04: info
tag_05: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Experiencia de Usuario (UX)
area_03: Diseño y prototipado Web
area_04: Desarrollo Front-end
area_05: Diseño de Información
area_06: Diseño Gráfico

cover_image: "/assets/images/cover-chilemarca.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Plataforma web"
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/screen-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/screen-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/chilemarca/screen-03.jpg"
  - title:
    description:
    columns: 2
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/view-01.png"
         - url: "/assets/images/chilemarca/view-02.png"
  - title: "Gráficas para compartir resultados"
    description:
    columns: 2
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/share-01.png"
         - url: "/assets/images/chilemarca/share-02.png"
  - title: "Gráficas redes sociales"
    description:
    columns: 2
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/gif-01.gif"
         - url: "/assets/images/chilemarca/gif-02.gif"
  - title:
    description:
    columns: 3
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/chilemarca/graphic-01.jpg"
         - url: "/assets/images/chilemarca/graphic-02.jpg"
         - url: "/assets/images/chilemarca/graphic-03.jpg"
  - title:
    description:
    columns: 3
    images:
         - url: "/assets/images/chilemarca/graphic-04.jpg"
         - url: "/assets/images/chilemarca/graphic-05.jpg"
         - url: "/assets/images/chilemarca/graphic-06.jpg"
---
