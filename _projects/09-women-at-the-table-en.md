---
layout: project
lang: en
visible_home: true
title: Women At The Table | Lissett García Albornoz
permalink: /women-at-the-table/

label: Politics & Technology
name: Women At The Table
subtitle: A+ Alliance for inclusive algorithms

versions: Year
years: '2019'
category: Implemented by Women At The Table and Ciudadanía Inteligente in
country: Europe and Latin America

tag_01: art
tag_02: identity
tag_03: web
tag_04: frontend
tag_05: editorial
tag_06: graphic

role: My role
area_01: Art Direction
area_02: Branding
area_03: User Experience (UX)
area_04: Web Design & Prototyping
area_05: Front-end Development
area_06: Editorial Design
area_07: Graphic Design

cover_image: "/assets/images/cover-alliance.jpg"
hero_image_desktop: "/assets/images/alliance/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/alliance/welcome-mobile.jpg"

lead:

blocks:
  - title: "Art Direction"
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/alliance/art.png"
  - title: "Web Platform"
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-03.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-04.png"                 
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/alliance/view-01.png"
         - url: "/assets/images/alliance/view-02.png"
         - url: "/assets/images/alliance/view-03.png"         
  - title: "Graphics"
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/alliance/graphic-01.png"
         - url: "/assets/images/alliance/graphic-02.png"
         - url: "/assets/images/alliance/graphic-03.png"
  - title: "Stickers"
    description:
    columns: 2
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/alliance/sticker-01.png"
         - url: "/assets/images/alliance/sticker-02.png"
  - title: "Brochure"
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/brochure-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/brochure-02.png"         
---
