---
layout: project
lang: es
visible_home: true
title: Women At The Table | Lissett García Albornoz
permalink: /women-at-the-table-es/

label: Tecnología y Política
name: Women At The Table
subtitle: Alianza A+ por Algoritmos Inclusivos

versions: Año
years: '2019'
category: Implementado por Women At The Table y Ciudadanía Inteligente en
country: Europa y Latinoamérica

tag_01: art
tag_02: identity
tag_03: web
tag_04: frontend
tag_05: editorial
tag_06: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Diseño de Identidad
area_03: Experiencia de Usuario (UX)
area_04: Diseño y prototipado Web
area_05: Desarrollo Front-end
area_06: Diseño Editorial
area_07: Diseño Gráfico

cover_image: "/assets/images/cover-alliance.jpg"
hero_image_desktop: "/assets/images/alliance/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/alliance/welcome-mobile.jpg"

lead:

blocks:
  - title: "Dirección de Arte"
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/alliance/art.png"
  - title: "Plataforma web"
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-03.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/screen-04.png"                 
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/alliance/view-01.png"
         - url: "/assets/images/alliance/view-02.png"
         - url: "/assets/images/alliance/view-03.png"         
  - title: "Productos gráficos"
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/alliance/graphic-01.png"
         - url: "/assets/images/alliance/graphic-02.png"
         - url: "/assets/images/alliance/graphic-03.png"
  - title: "Stickers"
    description:
    columns: 2
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/alliance/sticker-01.png"
         - url: "/assets/images/alliance/sticker-02.png"
  - title: "Brochure"
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/brochure-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/alliance/brochure-02.png"         
---
