---
layout: project
lang: en
visible_home: true
title: Open City Halls | Lissett García Albornoz
permalink: /open-city-halls/

label: Openness & Transparency
name: Open City Halls
subtitle: Open Municipalities Network in Latin America

versions: Year
years: '2019 and 2020'
category: Implemented by Ciudadanía Inteligente in
country: Latin America

lead:
tag_01: art
tag_02: web
tag_03: frontend
tag_04: editorial
tag_05: graphic

role: My role
area_01: Art Direction
area_02: User Experience (UX)
area_03: Web Design & Prototyping
area_04: Front-end Development
area_05: Editorial Design
area_06: Graphic Design

cover_image: "/assets/images/cover-abre2020.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Web platform"
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/screen.png"                
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2020/view-01.png"
         - url: "/assets/images/abre2020/view-02.png"
         - url: "/assets/images/abre2020/view-03.png"         
  - title: "Graphics"
    description:
    columns: 4
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2020/graphic-01.png"
         - url: "/assets/images/abre2020/graphic-02.png"
         - url: "/assets/images/abre2020/graphic-03.png"
         - url: "/assets/images/abre2020/graphic-04.png"
  - title: "Brochure"
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/brochure-01.png"
  - title:
    description:
    img-margin: "img-mb-120"
    columns: 1
    images:
         - url: "/assets/images/abre2020/brochure-02.png"
  - title: "Book"
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-03.jpg"
  - title:
    description:
    img-margin: "img-mb-120"
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-04.jpg"
  - title: "Merchandising"
    description:
    img-margin: "img-mb-120"
    columns: 2
    images:
         - url: "/assets/images/abre2020/bag.png"
         - url: "/assets/images/abre2020/notebook.png"
---
