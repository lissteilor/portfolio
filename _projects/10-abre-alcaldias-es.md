---
layout: project
lang: es
visible_home: true
title: Abre Alcaldías | Lissett García Albornoz
permalink: /abre-alcaldias/

label: Apertura y Transparencia
name: AbreAlcaldías
subtitle: Red de municipios abiertos en Latinoamérica

versions: Año
years: '2019 y 2020'
category: Implementado por Ciudadanía Inteligente en
country: Latinoamérica

lead:
tag_01: art
tag_02: web
tag_03: frontend
tag_04: editorial
tag_05: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Experiencia de Usuario
area_03: Diseño y prototipado Web
area_04: Desarrollo Front-end
area_05: Diseño Editorial
area_06: Diseño Gráfico

cover_image: "/assets/images/cover-abre2020.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Plataforma web"
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/screen.png"                
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2020/view-01.png"
         - url: "/assets/images/abre2020/view-02.png"
         - url: "/assets/images/abre2020/view-03.png"         
  - title: "Productos gráficos"
    description:
    columns: 4
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2020/graphic-01.png"
         - url: "/assets/images/abre2020/graphic-02.png"
         - url: "/assets/images/abre2020/graphic-03.png"
         - url: "/assets/images/abre2020/graphic-04.png"
  - title: "Brochure"
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/brochure-01.png"
  - title:
    description:
    img-margin: "img-mb-120"
    columns: 1
    images:
         - url: "/assets/images/abre2020/brochure-02.png"
  - title: "Libro"
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-03.jpg"
  - title:
    description:
    img-margin: "img-mb-120"
    columns: 1
    images:
         - url: "/assets/images/abre2020/book-04.jpg"
  - title: "Merchandising"
    description:
    img-margin: "img-mb-120"
    columns: 2
    images:
         - url: "/assets/images/abre2020/bag.png"
         - url: "/assets/images/abre2020/notebook.png"
---
