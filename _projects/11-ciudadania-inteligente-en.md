---
layout: project
lang: en
visible_home: false
title: Ciudadanía Inteligente | Lissett García Albornoz
permalink: /ciudadania-inteligente/

label: Politics & Technology
name: Ciudadanía Inteligente
subtitle: Brand identity Redesign

versions: Año
years: '2020'
category: Implemented by Ciudadanía Inteligente in
country: Latin America

lead:
tag_01: art
tag_02: identity
tag_03: web
tag_04: frontend
tag_05: editorial
tag_06: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Diseño de Identidad
area_03: Diseño y prototipado Web
area_04: Desarrollo Front-end
area_05: Diseño Editorial
area_06: Diseño Gráfico

designer_01: Natalia Matayoshi
designer_02: Pilar Grant

cover_image: "/assets/images/cover-alliance.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"
---
