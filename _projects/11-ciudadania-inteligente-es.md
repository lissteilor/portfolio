---
layout: project
lang: es
visible_home: false
title: Ciudadanía Inteligente 2020 | Lissett García Albornoz
permalink: /ciudadania-inteligente-2020/

label: Tecnología y Política
name: Ciudadanía Inteligente
subtitle: Cambio de imagen 2020

versions: Año
years: '2020'
category: Implementado por Ciudadanía Inteligente en
country: Latinoamérica

lead:
tag_01: art
tag_02: identity
tag_03: web
tag_04: frontend
tag_05: editorial
tag_06: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Diseño de Identidad
area_03: Diseño y prototipado Web
area_04: Desarrollo Front-end
area_05: Diseño Editorial
area_06: Diseño Gráfico

designer_01: Natalia Matayoshi
designer_02: Pilar Grant

cover_image: "/assets/images/cover-alliance.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"
---
