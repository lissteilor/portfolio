---
layout: project
lang: en
visible_home: true
title: From Saying to the Fact | Lissett García Albornoz
permalink: /from-saying-to-the-fact/

label: Openness & Transparency
name: From Saying to the Fact
subtitle: Legislative monitoring of presidential promises

versions: Versions
years: '2020, 2019, 2018, 2017 and 2016'
category: Implemented by Ciudadanía Inteligente in
country: Chile

lead:
tag_01: art
tag_02: web
tag_03: frontend
tag_04: info
tag_05: graphic

role: My role
area_01: Art Direction
area_02: User Experience (UX)
area_03: Web Design & Prototyping
area_04: Front-end Development
area_05: Infographic
area_06: Graphic Design

cover_image: "/assets/images/cover-ddah.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Pictograms"
    description:
    img-margin: "img-mb-120"
    columns: 1
    images:
         - url: "/assets/images/ddah/pictograms.png"
  - title: "Web Platform"
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-03.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-04.png"       
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/ddah/view-01.png"
         - url: "/assets/images/ddah/view-02.png"
         - url: "/assets/images/ddah/view-03.png"
  - title: "Graphics"
    description:
    columns: 4
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/ddah/graphic-01.png"
         - url: "/assets/images/ddah/graphic-02.png"
         - url: "/assets/images/ddah/graphic-03.png"
         - url: "/assets/images/ddah/graphic-04.png"                                 

---
