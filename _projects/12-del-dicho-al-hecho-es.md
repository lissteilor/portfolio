---
layout: project
lang: es
visible_home: true
title: Del Dicho al Hecho | Lissett García Albornoz
permalink: /del-dicho-al-hecho/

label: Apertura y Transparencia
name: Del Dicho al Hecho
subtitle: Seguimiento Legislativo de Promesas Presidenciales

versions: Versiones
years: '2020, 2019, 2018, 2017 y 2016'
category: Implementado por Ciudadanía Inteligente en
country: Chile

lead:
tag_01: art
tag_02: web
tag_03: frontend
tag_04: info
tag_05: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Experiencia de Usuario (UX)
area_03: Diseño y prototipado Web
area_04: Desarrollo Front-end
area_05: Diseño de Información
area_06: Diseño Gráfico

cover_image: "/assets/images/cover-ddah.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Pictogramas"
    description:
    img-margin: "img-mb-120"
    columns: 1
    images:
         - url: "/assets/images/ddah/pictograms.png"
  - title: "Plataforma web"
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-03.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/ddah/screen-04.png"       
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/ddah/view-01.png"
         - url: "/assets/images/ddah/view-02.png"
         - url: "/assets/images/ddah/view-03.png"
  - title: "Gráficas"
    description:
    columns: 4
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/ddah/graphic-01.png"
         - url: "/assets/images/ddah/graphic-02.png"
         - url: "/assets/images/ddah/graphic-03.png"
         - url: "/assets/images/ddah/graphic-04.png"                                 

---
