---
layout: project
lang: en
visible_home: true
title: Rise Your Hand | Lissett García Albornoz
permalink: /rise-your-hand/

label: Citizen Participation
name: Rise Your Hand
subtitle: Child and youth participation

versions: Years
years: '2018 and 2017'
category: Implemented by Ciudadanía Inteligente in
country: Chile and Spain

lead:
tag_01: research
tag_02: facilitation
tag_03: art
tag_04: identity
tag_05: editorial
tag_06: web
tag_07: frontend
tag_08: graphic

role: My role
area_01: User Research
area_02: Facilitation
area_03: Art Direction
area_04: Branding
area_05: Editorial Design
area_06: User Experience (UX)
area_07: Web Design & Prototyping
area_08: Front-end Development
area_09: Graphic Design

collaboration_01: Illustrations by
designer_01: 'Antonia Casali'

cover_image: "/assets/images/cover-llm.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Art Direction"
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/graphic-01.png"
  - title:
    description:
    columns: 3
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/llm/graphic-02.png"
         - url: "/assets/images/llm/graphic-03.png"
         - url: "/assets/images/llm/graphic-04.png"
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/llm/graphic-05.png"
         - url: "/assets/images/llm/graphic-06.png"
         - url: "/assets/images/llm/graphic-07.png"                 
  - title: "Web platform"
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/screen-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/screen-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/screen-03.jpg"               
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/llm/view-01.png"
         - url: "/assets/images/llm/view-02.png"
         - url: "/assets/images/llm/view-03.png"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/llm/picture-01.jpg"
         - url: "/assets/images/llm/picture-02.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/llm/picture-03.jpg"
         - url: "/assets/images/llm/picture-04.jpg"
  - title:
    description:
    columns: 2
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/llm/picture-05.jpg"
         - url: "/assets/images/llm/picture-06.jpg"
  - title: "Book"
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/book-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/book-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/book-03.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/book-04.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/book-05.jpg"
  - title:
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/llm/book-06.jpg"
  - title: "Manual"
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/book-07.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/book-08.jpg"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/llm/book-09.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/llm/book-10.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/llm/book-11.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/llm/book-12.png"
  - title: "Brochure"
    description:
    columns: 1
    images:
         - url: "/assets/images/llm/brochure.jpg"
  - title:
    description:
    columns: 4
    images:
         - url: "/assets/images/llm/congress-01.jpg"
         - url: "/assets/images/llm/congress-02.jpg"
         - url: "/assets/images/llm/congress-03.jpg"
         - url: "/assets/images/llm/congress-04.jpg"
  - title:
    description:
    columns: 4
    images:
         - url: "/assets/images/llm/congress-05.jpg"
         - url: "/assets/images/llm/congress-06.jpg"
         - url: "/assets/images/llm/congress-07.jpg"
         - url: "/assets/images/llm/congress-08.jpg"
  - title:
    description:
    columns: 4
    images:
         - url: "/assets/images/llm/congress-09.jpg"
         - url: "/assets/images/llm/congress-10.jpg"
         - url: "/assets/images/llm/congress-11.jpg"
         - url: "/assets/images/llm/congress-12.jpg"
  - title:
    description:
    columns: 4
    images:
         - url: "/assets/images/llm/congress-13.jpg"
         - url: "/assets/images/llm/congress-14.jpg"
         - url: "/assets/images/llm/congress-15.jpg"
         - url: "/assets/images/llm/congress-16.jpg"
  - title:
    description:
    columns: 4
    images:
         - url: "/assets/images/llm/congress-17.jpg"
         - url: "/assets/images/llm/congress-18.jpg"
---
