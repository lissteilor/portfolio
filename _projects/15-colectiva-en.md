---
layout: project
lang: en
visible_home: true
title: Colectiva | Lissett García Albornoz
permalink: /colectiva/

label: Activism
name: Colectiva
subtitle: Latin American summit for young activists

versions: Version
years: '2019'
category: Implemented by Ciudadanía Inteligente in
country: Latin America

lead:
tag_01: art
tag_02: identity
tag_03: web
tag_04: frontend
tag_05: graphic

role: My role
area_01: Art Direction
area_02: Branding
area_03: User Experience (UX)
area_04: Web Design & Prototyping
area_05: Front-end Development
area_06: Graphic Design

cover_image: "/assets/images/cover-colectiva.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Merchandising"
    description:
    columns: 3
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/colectiva/bag.jpg"
         - url: "/assets/images/colectiva/lanyard.jpg"
         - url: "/assets/images/colectiva/poster.jpg"
  - title: "Web platform"
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/screen-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/screen-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/screen-03.jpg"         
  - title:
    description:
    columns: 2
    img-shadow: "img-shadow"
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/colectiva/view-01.png"
         - url: "/assets/images/colectiva/view-03.png"                                
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-01.jpg"
         - url: "/assets/images/colectiva/picture-02.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-03.jpg"
         - url: "/assets/images/colectiva/picture-04.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-05.jpg"
         - url: "/assets/images/colectiva/picture-06.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-07.jpg"
         - url: "/assets/images/colectiva/picture-08.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-09.jpg"
         - url: "/assets/images/colectiva/picture-10.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-11.jpg"
         - url: "/assets/images/colectiva/picture-12.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-13.jpg"
         - url: "/assets/images/colectiva/picture-14.jpg"    
---
