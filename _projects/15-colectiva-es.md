---
layout: project
lang: es
visible_home: true
title: Colectiva | Lissett García Albornoz
permalink: /colectiva-es/

label: Activismo
name: Colectiva
subtitle: Encuentro latinoamericano de jóvenes activistas

versions: Versión
years: '2019'
category: Implementado por Ciudadanía Inteligente en
country: Latinoamérica

lead:
tag_01: art
tag_02: identity
tag_03: web
tag_04: frontend
tag_05: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Diseño de Identidad
area_03: Experiencia de Usuario (UX)
area_04: Diseño y prototipado Web
area_05: Desarrollo Front-end
area_06: Diseño Gráfico

cover_image: "/assets/images/cover-colectiva.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Merchandising"
    description:
    columns: 3
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/colectiva/bag.jpg"
         - url: "/assets/images/colectiva/lanyard.jpg"
         - url: "/assets/images/colectiva/poster.jpg"
  - title: "Plataforma web"
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/screen-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/screen-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/colectiva/screen-03.jpg"         
  - title:
    description:
    columns: 2
    img-shadow: "img-shadow"
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/colectiva/view-01.png"
         - url: "/assets/images/colectiva/view-03.png"                                
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-01.jpg"
         - url: "/assets/images/colectiva/picture-02.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-03.jpg"
         - url: "/assets/images/colectiva/picture-04.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-05.jpg"
         - url: "/assets/images/colectiva/picture-06.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-07.jpg"
         - url: "/assets/images/colectiva/picture-08.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-09.jpg"
         - url: "/assets/images/colectiva/picture-10.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-11.jpg"
         - url: "/assets/images/colectiva/picture-12.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/colectiva/picture-13.jpg"
         - url: "/assets/images/colectiva/picture-14.jpg"    
---
