---
layout: project
lang: es
visible_home: true
title: Escuela de Incidencia | Lissett García Albornoz
permalink: /escuela-de-incidencia/

label: Activism
name: Escuela de Incidencia
subtitle: Para jóvenes líderes latinoamericanos

versions: Versiones
years: '2019, 2018 y 2017'
category: Implementado por Ciudadanía Inteligente en
country: Bolivia, Brasil, Chile, Colombia, Ecuador, Guatemala, México y Perú

lead:
tag_01: art
tag_02: identity
tag_03: web
tag_04: frontend
tag_05: facilitation
tag_06: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Diseño de Identidad
area_03: Diseño y prototipado Web
area_04: Desarrollo Front-end
area_05: Facilitación
area_06: Diseño Gráfico

cover_image: "/assets/images/cover-edi.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Pictogramas"
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/edi/icons.png"
  - title: "Merchandising"
    description:
    columns: 3
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/edi/bag.jpg"
         - url: "/assets/images/edi/merchandising.jpg"
         - url: "/assets/images/edi/poster.jpg"  
  - title: "Plataforma web"
    description:
    columns: 1
    images:
         - url: "/assets/images/edi/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/edi/screen.jpg"                
  - title:
    description:
    columns: 2
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/edi/view-01.png"
         - url: "/assets/images/edi/view-02.png"
  - title: "Gráficas"
    description:
    columns: 3
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/edi/gif-01.gif"
         - url: "/assets/images/edi/gif-02.gif"
         - url: "/assets/images/edi/gif-03.gif"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/edi/picture-01.jpg"
         - url: "/assets/images/edi/picture-02.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/edi/picture-03.jpg"
         - url: "/assets/images/edi/picture-04.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/edi/picture-05.jpg"
         - url: "/assets/images/edi/picture-06.jpg"
  - title:
    description:
    columns: 2
    images:
         - url: "/assets/images/edi/picture-07.jpg"
         - url: "/assets/images/edi/picture-08.jpg"
  - title:
    description:
    columns: 2
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/edi/picture-09.jpg"
         - url: "/assets/images/edi/picture-10.jpg"         
  - title: "Manual"
    description:
    columns: 1
    images:
         - url: "/assets/images/edi/book-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/edi/book-02.jpg"
  - title:
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/edi/book-03.jpg"
  - title: "Plotters"
    description:
    columns: 1
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/edi/plotter.png"
---
