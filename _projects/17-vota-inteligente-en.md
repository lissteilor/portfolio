---
layout: project
lang: en
visible_home: true
title: Vote Smart | Lissett García Albornoz
permalink: /vote-smart/

label: Citizen Participation
name: Vote Smart
subtitle: Citizen proposals to presidential candidate programs

versions: Version
years: '2017'
category: Implemented by Ciudadanía Inteligente in
country: Chile

lead:
tag_01: research
tag_02: facilitation
tag_03: editorial
tag_04: frontend
tag_05: web
tag_06: graphic

role: My role
area_01: User Research
area_02: Facilitation
area_03: Editorial Design
area_04: User Experience (UX)
area_05: Front-end Development
area_06: Web Design & Prototyping
area_07: Graphic Design

collaboration_01: Illustrations by
designer_01: 'Antonia Casali'

cover_image: "/assets/images/cover-vota.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title: "Launch intervention"
    description:
    columns: 3
    images:
         - url: "/assets/images/vota/picture-01.jpg"
         - url: "/assets/images/vota/picture-02.jpg"
         - url: "/assets/images/vota/picture-03.jpg"
  - title:
    description:
    columns: 3
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/vota/picture-04.jpg"
         - url: "/assets/images/vota/picture-05.jpg"
         - url: "/assets/images/vota/picture-06.jpg"
  - title: "Web platform"
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/styleframe.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/screen-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/screen-02.png"
  - title:
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/vota/screen-03.png"
  - title: "Graphics"
    description:
    columns: 3
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/vota/gif-01.gif"
         - url: "/assets/images/vota/gif-02.gif"
         - url: "/assets/images/vota/gif-03.gif"
  - title:
    description:
    columns: 4
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/vota/graphic-01.png"
         - url: "/assets/images/vota/graphic-02.png"
         - url: "/assets/images/vota/graphic-03.png"
         - url: "/assets/images/vota/graphic-04.png"
  - title:
    description:
    columns: 4
    img-margin: "img-mb-120"
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/vota/graphic-05.png"
         - url: "/assets/images/vota/graphic-06.png"
         - url: "/assets/images/vota/graphic-07.png"
         - url: "/assets/images/vota/graphic-08.png"
  - title: "Citizen meetings"
    description:
    columns: 2
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/vota/picture-07.jpg"
         - url: "/assets/images/vota/picture-08.jpg"
  - title: "Manuals"
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/book-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/book-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/book-03.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/book-04.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/book-05.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/vota/book-06.png"                                                 
---
