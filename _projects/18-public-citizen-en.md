---
layout: project
lang: en
visible_home: true
title: Public Citizen | Lissett García Albornoz
permalink: /public-citizen/

label: Activism
name: Public Citizen
subtitle: Protecting health, safety and democracy

versions: Year
years: '2019'
category: Implemented by Public Citizen in
country: United States

lead:
tag_01: art
tag_02: editorial
tag_03: graphic

role: My role
area_01: Art Direction
area_02: Editorial Design
area_03: Graphic Design

cover_image: "/assets/images/cover-public.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-03.png"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-04.png"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-05.png"
  - title:
    description:
    columns: 1
    images:
        - url: "/assets/images/public/brochure-06.png"
---
