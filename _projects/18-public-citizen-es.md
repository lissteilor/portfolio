---
layout: project
lang: es
visible_home: true
title: Public Citizen | Lissett García Albornoz
permalink: /public-citizen-es/

label: Activismo
name: Public Citizen
subtitle: Derechos de los Consumidores

versions: Año
years: '2019'
category: Implementado por Public Citizen en
country: Estados Unidos

lead:
tag_01: art
tag_02: editorial
tag_03: graphic

role: Mi rol
area_01: Dirección de Arte
area_02: Diseño Editorial
area_03: Diseño Gráfico

cover_image: "/assets/images/cover-public.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-03.png"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-04.png"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/public/brochure-05.png"
  - title:
    description:
    columns: 1
    images:
        - url: "/assets/images/public/brochure-06.png"
---
