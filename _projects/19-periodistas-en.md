---
layout: project
lang: en
visible_home: false
title: Journalists and social networks in Chile | Lissett García Albornoz
permalink: /journalists/

name: Journalists and social networks in Chile
subtitle: National Report 2019

versions: Year
years: '2020'
category:
country: Chile

lead:
tag_01: editorial
tag_02: graphic

role: My role
area_01: Editorial Design
area_02: Graphic Design

cover_image: "/assets/images/cover-periodistas.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-00.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-02.jpg"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-03.jpg"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-04.jpg"
  - title:
    description:
    columns: 1
    images:
        - url: "/assets/images/periodistas/book-05.jpg"
---
