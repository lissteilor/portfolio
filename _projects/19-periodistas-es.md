---
layout: project
lang: es
visible_home: false
title: Periodistas y redes sociales en Chile | Lissett García Albornoz
permalink: /periodistas/

name: Periodistas y redes sociales en Chile
subtitle: Informe nacional 2019

versions: Año
years: '2020'
category:
country: Chile

lead:
tag_01: editorial
tag_02: graphic

role: Mi rol
area_01: Diseño Editorial
area_02: Diseño Gráfico

cover_image: "/assets/images/cover-periodistas.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-00.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-02.jpg"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-03.jpg"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/periodistas/book-04.jpg"
  - title:
    description:
    columns: 1
    images:
        - url: "/assets/images/periodistas/book-05.jpg"
---
