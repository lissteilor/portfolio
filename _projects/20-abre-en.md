---
layout: project
lang: en
visible_home: true
title: Abre | Lissett García Albornoz
permalink: /abre/

label: Openness & Transparency
name: Abre
subtitle: Open municipalities

versions: Año
years: '2016'
category: Graphic proposal for Ciudadanía Inteligente

lead:
tag_01: art
tag_02: graphic

role: My role
area_01: Art Direction
area_02: Graphic Design

cover_image: "/assets/images/cover-abre2015.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/abre2015/icons.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2015/slide-01.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2015/slide-02.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2015/slide-03.png"  
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2015/slide-04.png"  
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
         - url: "/assets/images/abre2015/slide-05.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-06.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-07.png"  
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-08.png"  
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-09.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-10.png"
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-11.png"  
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-12.png"  
  - title:
    description:
    columns: 1
    img-shadow: "img-shadow"
    images:
        - url: "/assets/images/abre2015/slide-13.png"            
---
