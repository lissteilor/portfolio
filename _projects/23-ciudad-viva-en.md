---
layout: project
lang: en
visible_home: true
title: Active Citizen | Lissett García Albornoz
permalink: /active-citizen/

label: Citizen Participation
name: Active Citizen
subtitle: Public participation in land use planning

versions: Year
years: '2014'
category: Service Internship, Academic study in
country: Chile

lead:
tag_01: research
tag_02: info
tag_03: graphic

role: My role
area_01: User Research
area_02: Infographic
area_03: Graphic Design

cover_image: "/assets/images/cover-zoning.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-03.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-04.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-05.png"
---
