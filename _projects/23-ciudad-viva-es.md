---
layout: project
lang: es
visible_home: true
title: Ciudad Viva | Lissett García Albornoz
permalink: /ciudad-viva-es/

label: Participación Ciudadana
name: Ciudad Viva
subtitle: Participación en Planes Reguladores Comunales

versions: Año
years: '2014'
category: Práctica de Servicio, Análisis académico en
country: Chile

lead:
tag_01: research
tag_02: info
tag_03: graphic

role: Mi rol
area_01: Investigación de Usuarios
area_02: Diseño de Información
area_03: Diseño Gráfico

cover_image: "/assets/images/cover-zoning.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-01.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-02.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-03.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-04.png"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/zoning/view-05.png"
---
