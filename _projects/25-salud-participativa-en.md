---
layout: project
lang: en
visible_home: true
title: Participatory Health  | Lissett García Albornoz
permalink: /participatory-health/

label: Citizen Participation
name: Participatory Health
subtitle: Political controversies in public health

versions: Year
years: '2013'
category: Academic study in
country: Chile

lead:
tag_01: research
tag_02: info
tag_03: graphic

role: My role
area_01: User Research
area_02: Infographic
area_03: Graphic Design

collaboration_01: Designed with
designer_01: Matías Echavarría

cover_image: "/assets/images/cover-salud.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/salud/project.png"
---
