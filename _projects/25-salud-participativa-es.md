---
layout: project
lang: es
visible_home: true
title: Salud Participativa | Lissett García Albornoz
permalink: /salud-participativa/

label: Participación Ciudadana
name: Salud Participativa
subtitle: Controversias políticas en salud pública

versions: Año
years: '2013'
category: Análisis académico en
country: Chile

lead:
tag_01: research
tag_02: info
tag_03: graphic

role: Mi rol
area_01: Investigación de Usuarios
area_02: Diseño de Información
area_03: Diseño Gráfico

collaboration_01: Diseñado en conjunto con
designer_01: Matías Echavarría

cover_image: "/assets/images/cover-salud.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    img-margin: "img-mb-120"
    images:
         - url: "/assets/images/salud/project.png"
---
