---
layout: project
lang: es
visible_home: true
title: Bordados | Lissett García Albornoz
permalink: /bordados/

name: Bordados
subtitle: Me apasiona bordar

versions: Años
years: '2019 y 2020'
category: Trabajos personales

lead:

cover_image: "/assets/images/cover-embroideries.jpg"
hero_image_desktop: "/assets/images/welcome-desktop.jpg"
hero_image_mobile: "/assets/images/welcome-mobile.jpg"

blocks:
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/embroideries/embroidery-01.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/embroideries/embroidery-02.jpg"
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/embroideries/embroidery-03.jpg"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/embroideries/embroidery-04.jpg"  
  - title:
    description:
    columns: 1
    images:
         - url: "/assets/images/embroideries/embroidery-05.jpg"
  - title:
    description:
    columns: 1
    images:
        - url: "/assets/images/embroideries/embroidery-06.jpg"
  - title:
    description:
    columns: 1
    images:
        - url: "/assets/images/embroideries/embroidery-07.jpg"
---
