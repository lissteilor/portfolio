---
layout: default
title: About
permalink: /about/
---

<div id="about" class="uk-section animated fadeIn delay-2s fast">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false" style="margin-top:100px;">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-3@m">
        <img src="/assets/images/about.jpg" alt="{% t about.image %}" title="{% t about.image %}" uk-img>
        <p>{% t about.mail %}</p>
        <p style="margin:-16px 0 0">{% t about.based %}</p>
        <p style="margin:0">{% t about.linkedin %}</p>
        <p style="margin:0">{% t about.github %}</p>
        <p style="margin:0">{% t about.gitlab %}</p>
        <p style="margin:0" class="uk-hidden">{% t about.cv %}</p>
      </div>
      <div class="uk-width-2-3@m">
        <p>{% t about.description_01 %}</p>
        <p>{% t about.description_03 %}</p>
        <p>{% t about.description_04 %}</p>
        <p>{% t about.description_05 %}</p>
        <p>{% t about.contact %}</p>
      </div>
    </div>
  </div>
</div>
