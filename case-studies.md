---
layout: default
title: Case Studies
permalink: /case-studies/
---

<div class="uk-section uk-flex uk-flex-center animated fadeIn delay-2s fast">
  <div class="uk-container uk-width-3-4@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
  <h1 class="font-700 animated fadeInUp" style="margin-top:70px;margin-bottom:50px">Case Studies</h1>

    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
          {% for c in site.casestudies %}
          {% if c.lang == site.lang %}
          {% if c.visible_home %}
          <div style="margin-bottom: 20px;">
            <div class="uk-inline-clip uk-transition-toggle uk-light" tabindex="0">
                <a href="{{c.url | prepend:site.baseurl }}"><img src="{{ c.cover_image | prepend:site.baseurl_root }}" class="uk-transition-scale-up uk-transition-opaque" alt="" /></a>
            </div>
            <h2 class="font-700" style="font-size: 1.5rem !important;margin-top: 20px;">{{ c.name }}</h2>
            <h3 style="margin: -8px 0 20px; font-size: 1rem !important;line-height: 1.2rem !important;">{{ c.subtitle }}</h3>
          </div>
         {% endif %}
         {% endif %}
         {% endfor %}
       </div>
  </div>
</div>
