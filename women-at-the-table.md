---
layout: project
title: Women At The Table
permalink: /women-at-the-table/
welcome_desktop: ../assets/images/welcome-desktop.jpg
welcome_mobile: ../assets/images/welcome-mobile.jpg
---

<div id="welcome-project" class="uk-visible@m uk-height-viewport uk-flex uk-flex-center uk-inline uk-flex-middle uk-background-cover uk-light animated fadeIn" uk-parallax="bgy: -100;" data-src="{{ page.welcome_desktop }}" uk-img>
  <div class="uk-container uk-width-5-6@m">
    <div class="uk-width-1-1@m">
      <h2 class="font-uppercase font-700 animated fadeInUp">{% t p13.title %}</h2>
      <h3 class="font-uppercase animated fadeInUp" style="margin:-10px 0 40px">{% t p13.subtitle %}</h3>
    </div>
    <div class="uk-width-1-1">
      <div class="uk-flex uk-flex-center animated fadeInUp" uk-grid>
        <div class="uk-width-3-4@m">
          <p>{% t p13.lead %}</p>
          <a class="icon-hero uk-visible@m" href="#project" uk-scroll>
            <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M48.7685 22.0412C48.3813 21.6507 47.921 21.3407 47.4137 21.1292C46.9064 20.9177 46.3622 20.8088 45.8126 20.8088C45.263 20.8088 44.7189 20.9177 44.2113 21.1292C43.7042 21.3407 43.2438 21.6507 42.8567 22.0412L29.1397 35.791V4.16659C29.1397 3.06134 28.7211 2.00175 27.9403 1.22037C27.1395 0.438979 26.1006 0 24.9964 0C23.8923 0 22.8334 0.438979 22.0526 1.22037C21.2718 2.00175 20.8332 3.06134 20.8332 4.16659V35.791L7.13617 22.0412C6.35222 21.2567 5.28895 20.8139 4.18027 20.8139C3.07139 20.8139 2.00833 21.2567 1.22437 22.0412C0.44042 22.8258 0 23.8899 0 24.9995C0 26.1091 0.44042 27.1732 1.22437 27.9578L22.0406 48.7907C22.4365 49.17 22.9034 49.4674 23.4144 49.6657C23.9128 49.8861 24.4516 50 24.9964 50C25.5413 50 26.0801 49.8861 26.5785 49.6657C27.0895 49.4674 27.5564 49.17 27.9523 48.7907L48.7685 27.9578C49.1387 27.5705 49.4685 27.1096 49.6798 26.6019C49.8912 26.0942 50 25.5496 50 24.9995C50 24.4495 49.8912 23.9049 49.6798 23.3971C49.4685 22.8894 49.1387 22.4286 48.7685 22.0412V22.0412Z" fill="white"/>
            </svg>
          </a>
        </div>
        <div class="uk-width-1-4@m">
          <p class="font-uppercase font-700">{% t p13.position %}</p>
          <ul class="uk-list" style="line-height:1;margin-top:-10px">
            <li>{% t p13.task_01 %}</li>
            <li>{% t p13.task_02 %}</li>
            <li>{% t p13.task_03 %}</li>
            <li>{% t p13.task_04 %}</li>
            <li>{% t p13.task_05 %}</li>
            <li>{% t p13.task_06 %}</li>
            <li>{% t p13.task_07 %}</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="welcome-project" class="uk-hidden@m uk-height-viewport uk-flex uk-flex-center uk-inline uk-flex-middle uk-background-cover uk-light animated fadeIn" uk-parallax="bgy: -100;" data-src="{{ page.welcome_desktop }}" uk-img>
  <div class="uk-container uk-width-5-6@m">
    <div class="uk-width-1-1@m">
      <h2 class="font-uppercase font-700 animated fadeInUp">{% t p13.title %}</h2>
      <h3 class="font-uppercase animated fadeInUp" style="margin:-10px 0 20px">{% t p13.subtitle %}</h3>
    </div>
    <div class="uk-width-1-1">
      <p style="margin:0 0 30px">{% t p13.lead %}</p>
    </div>
    <div class="uk-width-1-1">
      <p class="font-uppercase font-700" style="margin-bottom:-20px">{% t p13.position %}</p>
      <p>{% t p13.task_01 %} • {% t p13.task_02 %} • {% t p13.task_03 %} • {% t p13.task_04 %} • {% t p13.task_05 %} • {% t p13.task_06 %} • {% t p13.task_07 %}</p>
      <a class="icon-hero" href="#project" uk-scroll>
        <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M48.7685 22.0412C48.3813 21.6507 47.921 21.3407 47.4137 21.1292C46.9064 20.9177 46.3622 20.8088 45.8126 20.8088C45.263 20.8088 44.7189 20.9177 44.2113 21.1292C43.7042 21.3407 43.2438 21.6507 42.8567 22.0412L29.1397 35.791V4.16659C29.1397 3.06134 28.7211 2.00175 27.9403 1.22037C27.1395 0.438979 26.1006 0 24.9964 0C23.8923 0 22.8334 0.438979 22.0526 1.22037C21.2718 2.00175 20.8332 3.06134 20.8332 4.16659V35.791L7.13617 22.0412C6.35222 21.2567 5.28895 20.8139 4.18027 20.8139C3.07139 20.8139 2.00833 21.2567 1.22437 22.0412C0.44042 22.8258 0 23.8899 0 24.9995C0 26.1091 0.44042 27.1732 1.22437 27.9578L22.0406 48.7907C22.4365 49.17 22.9034 49.4674 23.4144 49.6657C23.9128 49.8861 24.4516 50 24.9964 50C25.5413 50 26.0801 49.8861 26.5785 49.6657C27.0895 49.4674 27.5564 49.17 27.9523 48.7907L48.7685 27.9578C49.1387 27.5705 49.4685 27.1096 49.6798 26.6019C49.8912 26.0942 50 25.5496 50 24.9995C50 24.4495 49.8912 23.9049 49.6798 23.3971C49.4685 22.8894 49.1387 22.4286 48.7685 22.0412V22.0412Z" fill="white"/>
        </svg>
      </a>
    </div>
  </div>
</div>

<div id="project">
</div>
